﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IntechPOS.UserInterfaces
{
    /// <summary>
    /// Interaction logic for CustomerPaymentWindow.xaml
    /// </summary>
    public partial class CustomerPaymentWindow : Window
    {
        public CustomerPaymentWindow()
        {
            InitializeComponent();

            UserControl usc = null;
            GridCalc.Children.Clear();
            usc = new CalculatorWindow();
            GridCalc.Children.Add(usc);
        }
    }
}
