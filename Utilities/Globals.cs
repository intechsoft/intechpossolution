﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntechPOS.Utilities
{
    public class Globals
    {
        /**
         * Create all Global variables here
         */
        public Globals() { 
        
        }

        public static LoggedUser loggedUser = new LoggedUser();
    }

    public class LoggedUser {
        public int UserID;
        public string Username;
        public string Password;
        public string DisplayUsername;
        public int UserRoleID;
        public int UserTypeID;
        public bool isValidUser = false;

        public LoggedUser() { }
    }
}
