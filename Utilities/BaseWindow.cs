﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntechPOS.Utilities
{
    public class BaseWindow
    {
        public BaseWindow() {

            if (!Globals.loggedUser.isValidUser) {
                // Move to login window
            }
        }
    }

    /*
     Create al your common properties here.
    Inherit this parent class to all window classs (Forms)
     */
}
